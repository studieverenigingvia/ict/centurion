module.exports = {
  "ignorePatterns": [".eslintrc.js", "vite.config.ts", "postcss.config.js"],
  "settings": {
    "react": {
      "version": "detect"
    }
  },
  "parserOptions": {
    "tsconfigRootDir": __dirname,
    "project": ["./tsconfig.json"]
  },
  "extends": [
      "plugin:react/recommended"
  ],
  "rules": {
    // suppress errors for missing 'import React' in files
   "react/react-in-jsx-scope": "off",
    // At some point these must be set to "error"
    "@typescript-eslint/no-unsafe-member-access": "warn",
    "@typescript-eslint/no-unsafe-assignment": "warn",
    "@typescript-eslint/no-unsafe-argument": "warn",
    "@typescript-eslint/no-misused-promises": "warn",
    "@typescript-eslint/ban-ts-comment": "warn"
  }
};
