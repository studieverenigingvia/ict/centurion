import ReactDOM from "react-dom";
import "./css/index.css";

import Centurion from "./components/Centurion";

ReactDOM.render(<Centurion />, document.getElementById("root"));
