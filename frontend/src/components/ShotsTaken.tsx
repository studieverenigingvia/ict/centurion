import { Progress } from "antd";
import { calculateRoomTime, useTimeline } from "../lib/Connection";
import { useUpdateAfterDelay } from "../util/hooks";

const ShotsTaken = () => {
  const timeline = useTimeline();
  useUpdateAfterDelay(1000);

  if (!timeline) {
    throw new TypeError("ShotsTaken without timeline");
  }
  const totalShots = timeline.getTotalShotCount();

  let taken = 0;

  const time = calculateRoomTime();
  const prevShot = timeline.eventBeforeTime(time, "shot");

  if (prevShot) {
    taken = prevShot.shotCount;
  } else {
    const nextShot = timeline.eventAfterTime(time, "shot");
    taken = nextShot ? nextShot.shotCount - 1 : taken;
  }

  return (
    <>
      <h1>Shots genomen:</h1>
      <Progress
        type="circle"
        percent={(taken / totalShots) * 100}
        format={() => `${taken} / ${totalShots}`}
        status="normal"
        strokeColor={"#304ba3"}
        strokeWidth={10}
      />
    </>
  );
};

export default ShotsTaken;
