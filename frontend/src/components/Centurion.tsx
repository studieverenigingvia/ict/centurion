import React, { useState } from "react";

import { useRoomRunningAndReadyChanged } from "../lib/Connection";
import Feed from "./Feed";
import Lobby from "./Lobby";

const Centurion = () => {
  const [currentUserReady, setCurrentUserReady] = useState(false);
  const room = useRoomRunningAndReadyChanged();
  const showFeed = (room?.readyToParticipate && currentUserReady) || false;

  return (
    <section className="content">
      {showFeed ? (
        <Feed />
      ) : (
        <Lobby
          currentUserReady={currentUserReady}
          onCurrentUserReadyChange={(b: boolean) => setCurrentUserReady(b)}
        />
      )}
    </section>
  );
};

export default Centurion;
