import { Col, Row } from "antd";

import type { TimestampEvent } from "../types/types";

import "../css/feed.css";
import shot from "../img/shot.png";
import song from "../img/song.png";
import talk from "../img/talk.png";
import time from "../img/time.png";

export interface FeedItemProps {
  item: TimestampEvent;
}

const images = {
  shot,
  song,
  talk,
  time,
};

const FeedItem = ({ item }: FeedItemProps) => {
  return (
    <Row align="middle" className="feed-item">
      <Col span={11} className="feed-item__title">
        {item.text[0]}
      </Col>
      <Col span={2} className="feed-item__emoji">
        <img alt={item.type} src={images[item.type]} />
      </Col>
      <Col span={11} className="feed-item__desc">
        {item.text[1]}
      </Col>
    </Row>
  );
};

export default FeedItem;
