import { useEffect, useState } from "react";

export function useUpdateAfterDelay(delay: number) {
  const [, timedUpdateSet] = useState(0);
  useEffect(() => {
    const timeoutId = setTimeout(() => {
      timedUpdateSet((v) => v + 1);
    }, delay);
    return () => clearTimeout(timeoutId);
  });
}

export function useResize() {
  const [dimensions, setDimensions] = useState({
    height: window.innerHeight,
    width: window.innerWidth,
  });

  useEffect(() => {
    const listener = (ev: UIEvent) => {
      setDimensions({
        height: window.innerHeight,
        width: window.innerWidth,
      });
    };
    window.addEventListener("resize", listener);

    return () => {
      window.removeEventListener("resize", listener);
    };
  });

  return dimensions;
}
