module.exports = {
  "ignorePatterns": [".eslintrc.js"],
  "parserOptions": {
    "tsconfigRootDir": __dirname,
    "project": ["./tsconfig.json"]
  }
};
