// FIXME: dedupes these types from front-end
export interface TimelineItem {
  id?: string;
  timestamp: number;
  events: TimestampEvent[];
}

export const EVENT_PRIORITY: EventType[] = ["shot", "talk", "time", "song"];
export type EventType = "talk" | "shot" | "song" | "time";

interface TimestampEventBase {
  id?: string;
  type: EventType;
  text: string[];
}

interface TimestampEventShot extends TimestampEventBase {
  type: "shot";
  shotCount: number;
}

export type TimestampEvent = TimestampEventBase | TimestampEventShot;

const timelines: {
  timelines: {
    name: string;
    songFile: string;
    feed: TimelineItem[];
  }[];
} = {
  timelines: [
    {
      name: "Centurion",
      songFile: "songs/centurion.m4a",
      feed: [
        {
          timestamp: 0,
          events: [
            {
              type: "talk",
              text: ["Zet je schrap!", "We gaan zo beginnen"],
            },
          ],
        },
        {
          timestamp: 42,
          events: [
            {
              type: "talk",
              text: [
                "Daar gaan we!",
                "Dit was het startsein, je hoeft nog niet te drinken",
              ],
            },
            {
              type: "song",
              text: ["Nena", "99 Luftballons"],
            },
          ],
        },
        {
          timestamp: 108,
          events: [
            {
              type: "shot",
              text: ["De eerste!", "Nog 99 shotjes"],
              shotCount: 1,
            },
          ],
        },
        {
          timestamp: 148,
          events: [
            {
              type: "song",
              text: ["Hermes House Band", "Country Roads"],
            },
          ],
        },
        {
          timestamp: 167,
          events: [
            {
              type: "shot",
              text: ["Nummertje twee!", "Nog 98 shotjes"],
              shotCount: 2,
            },
          ],
        },
        {
          timestamp: 185,
          events: [
            {
              type: "song",
              text: ["Vinzzent", "Dromendans"],
            },
          ],
        },
        {
          timestamp: 223,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 97 shotjes"],
              shotCount: 3,
            },
            {
              type: "song",
              text: ["Linda, Roos & Jessica", "Ademnood"],
            },
          ],
        },
        {
          timestamp: 283,
          events: [
            {
              type: "shot",
              text: ["Niet gooien!", "Nog 96 shotjes"],
              shotCount: 4,
            },
            {
              type: "song",
              text: [
                "Peter de Koning",
                "Het is altijd lente in de ogen van de tandarts-assistente",
              ],
            },
          ],
        },
        {
          timestamp: 340,
          events: [
            {
              type: "shot",
              text: ["Lustrum!", "Nog 95 shotjes"],
              shotCount: 5,
            },
            {
              type: "song",
              text: ["Liquido", "Narcotic"],
            },
          ],
        },
        {
          timestamp: 412,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 94 shotjes"],
              shotCount: 6,
            },
            {
              type: "song",
              text: ["Snoop Dogg feat. Pharrell", "Drop It Like It's Hot"],
            },
          ],
        },
        {
          timestamp: 449,
          events: [
            {
              type: "song",
              text: ["M.O.P.", "Ante Up"],
            },
          ],
        },
        {
          timestamp: 459,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 93 shotjes"],
              shotCount: 7,
            },
          ],
        },
        {
          timestamp: 514,
          events: [
            {
              type: "shot",
              text: ["Trek een ad!", "Nog 92 shotjes"],
              shotCount: 8,
            },
            {
              type: "song",
              text: ["Los Del Rio", "Macarena"],
            },
          ],
        },
        {
          timestamp: 560,
          events: [
            {
              type: "song",
              text: ["Spice Girls", "Wannabe"],
            },
          ],
        },
        {
          timestamp: 574,
          events: [
            {
              type: "shot",
              text: ["We zijn er nog lang niet!", "Nog 91 shotjes"],
              shotCount: 9,
            },
          ],
        },
        {
          timestamp: 617,
          events: [
            {
              type: "song",
              text: [
                "Major Lazer feat. Busy Signal, The Flexican & FS Green",
                "Watch Out For This (Bumaye)",
              ],
            },
          ],
        },
        {
          timestamp: 635,
          events: [
            {
              type: "shot",
              text: ["Nummer tien!", "Nog 90 shotjes"],
              shotCount: 10,
            },
          ],
        },
        {
          timestamp: 647,
          events: [
            {
              type: "time",
              text: ["Nog 90 minuten!", "Geef alles, behalve op"],
            },
          ],
        },
        {
          timestamp: 650,
          events: [
            {
              type: "song",
              text: ["André van Duin", "Er staat een paard in de gang"],
            },
          ],
        },
        {
          timestamp: 667,
          events: [
            {
              type: "song",
              text: ["Lil Kleine & Ronnie Flex", "Drank & Drugs"],
            },
          ],
        },
        {
          timestamp: 684,
          events: [
            {
              type: "song",
              text: ["Kabouter Plop", "Kabouterdans"],
            },
          ],
        },
        {
          timestamp: 699,
          events: [
            {
              type: "shot",
              text: ["Met vriendelijke toet!", "Nog 89 shotjes"],
              shotCount: 11,
            },
          ],
        },
        {
          timestamp: 725,
          events: [
            {
              type: "song",
              text: ["K3", "Alle kleuren"],
            },
          ],
        },
        {
          timestamp: 756,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 88 shotjes"],
              shotCount: 12,
            },
          ],
        },
        {
          timestamp: 757,
          events: [
            {
              type: "song",
              text: ["Kinderen voor Kinderen", "Tietenlied"],
            },
          ],
        },
        {
          timestamp: 812,
          events: [
            {
              type: "shot",
              text: ["Ongeluksshotje 13!", "Nog 87 shotjes"],
              shotCount: 13,
            },
          ],
        },
        {
          timestamp: 814,
          events: [
            {
              type: "song",
              text: ["Guus Meeuwis", "Het dondert en het bliksemt"],
            },
          ],
        },
        {
          timestamp: 874,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 86 shotjes"],
              shotCount: 14,
            },
          ],
        },
        {
          timestamp: 876,
          events: [
            {
              type: "song",
              text: ["Harry Vermeegen", "1-2-3-4 Dennis bier"],
            },
          ],
        },
        {
          timestamp: 906,
          events: [
            {
              type: "song",
              text: ["Puhdys", "Hey, wir woll’n die Eisbär'n sehn!"],
            },
          ],
        },
        {
          timestamp: 935,
          events: [
            {
              type: "shot",
              text: ["Fünfzehn!", "Nog 85 shotjes"],
              shotCount: 15,
            },
          ],
        },
        {
          timestamp: 966,
          events: [
            {
              type: "song",
              text: ["DJ Ötzi", "Burger Dance"],
            },
          ],
        },
        {
          timestamp: 995,
          events: [
            {
              type: "shot",
              text: ["Toet!", "Nog 84 shotjes"],
              shotCount: 16,
            },
          ],
        },
        {
          timestamp: 996,
          events: [
            {
              type: "song",
              text: ["Mickie Krause", "Hütte auf der Alm"],
            },
          ],
        },
        {
          timestamp: 1030,
          events: [
            {
              type: "song",
              text: ["Ali B & Yes-R & The Partysquad", "Rampeneren"],
            },
          ],
        },
        {
          timestamp: 1046,
          events: [
            {
              type: "shot",
              text: ["Zuipen!", "Nog 83 shotjes"],
              shotCount: 17,
            },
          ],
        },
        {
          timestamp: 1107,
          events: [
            {
              type: "shot",
              text: ["18, legaal!", "Nog 82 shotjes"],
              shotCount: 18,
            },
            {
              type: "song",
              text: ["Martin Solveig", "Intoxicated"],
            },
          ],
        },
        {
          timestamp: 1137,
          events: [
            {
              type: "song",
              text: ["Nicki Minaj", "Starships"],
            },
          ],
        },
        {
          timestamp: 1173,
          events: [
            {
              type: "shot",
              text: ["Laatste shotje als tiener!", "Nog 81 shotjes"],
              shotCount: 19,
            },
          ],
        },
        {
          timestamp: 1222,
          events: [
            {
              type: "song",
              text: ["2Unlimited", "Get Ready For This"],
            },
          ],
        },
        {
          timestamp: 1233,
          events: [
            {
              type: "time",
              text: ["Nog 80 minuten!", "Al 19 shotjes achter de rug"],
            },
          ],
        },
        {
          timestamp: 1235,
          events: [
            {
              type: "shot",
              text: ["Adje!", "Nog 80 shotjes"],
              shotCount: 20,
            },
          ],
        },
        {
          timestamp: 1275,
          events: [
            {
              type: "song",
              text: ["The Village People", "YMCA"],
            },
          ],
        },
        {
          timestamp: 1288,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 79 shotjes"],
              shotCount: 21,
            },
          ],
        },
        {
          timestamp: 1348,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 78 shotjes"],
              shotCount: 22,
            },
          ],
        },
        {
          timestamp: 1350,
          events: [
            {
              type: "song",
              text: ["Carly Rae Jepsen ft Owl City", "It's Always A Good Time"],
            },
          ],
        },
        {
          timestamp: 1395,
          events: [
            {
              type: "song",
              text: ["Avicii", "Levels"],
            },
          ],
        },
        {
          timestamp: 1411,
          events: [
            {
              type: "shot",
              text: ["Claxon!", "Nog 77 shotjes"],
              shotCount: 23,
            },
          ],
        },
        {
          timestamp: 1456,
          events: [
            {
              type: "song",
              text: ["Flo-Rida feat. T-Pain", "Low"],
            },
          ],
        },
        {
          timestamp: 1471,
          events: [
            {
              type: "shot",
              text: ["En nu even op standje maximaal!", "Nog 76 shotjes"],
              shotCount: 24,
            },
          ],
        },
        {
          timestamp: 1486,
          events: [
            {
              type: "song",
              text: ["Taio Cruz", "Hangover"],
            },
          ],
        },
        {
          timestamp: 1522,
          events: [
            {
              type: "talk",
              text: [
                "Dit is nog het rustige stukje!",
                "Zet hem maar op de bonk-bonk",
              ],
            },
          ],
        },
        {
          timestamp: 1530,
          events: [
            {
              type: "shot",
              text: ["Halve Abraham!", "Nog 75 shotjes"],
              shotCount: 25,
            },
          ],
        },
        {
          timestamp: 1545,
          events: [
            {
              type: "song",
              text: ["LMFAO", "Party Rock Anthem"],
            },
          ],
        },
        {
          timestamp: 1594,
          events: [
            {
              type: "shot",
              text: ["Hoch die Hände!", "Nog 74 shotjes"],
              shotCount: 26,
            },
            {
              type: "song",
              text: ["Hans Entertainment vs. Finger & Kadel", "Hoch die Hände"],
            },
          ],
        },
        {
          timestamp: 1623,
          events: [
            {
              type: "song",
              text: ["Galantis", "No Money"],
            },
          ],
        },
        {
          timestamp: 1653,
          events: [
            {
              type: "shot",
              text: ["Voel je 'm al?", "Nog 73 shotjes"],
              shotCount: 27,
            },
          ],
        },
        {
          timestamp: 1683,
          events: [
            {
              type: "song",
              text: ["Kid Cudi", "Pursuit of Happiness (Steve Aoki remix)"],
            },
          ],
        },
        {
          timestamp: 1712,
          events: [
            {
              type: "shot",
              text: ["Project X!", "Nog 72 shotjes"],
              shotCount: 28,
            },
          ],
        },
        {
          timestamp: 1741,
          events: [
            {
              type: "song",
              text: ["Yeah Yeah Yeahs", "Heads Will Roll (A-Trak remix)"],
            },
          ],
        },
        {
          timestamp: 1769,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 71 shotjes"],
              shotCount: 29,
            },
          ],
        },
        {
          timestamp: 1814,
          events: [
            {
              type: "song",
              text: ["Michael Calfan", "Resurrection"],
            },
          ],
        },
        {
          timestamp: 1825,
          events: [
            {
              type: "time",
              text: ["Nog 70 minuten!", "Half uurtje zit erop"],
            },
          ],
        },
        {
          timestamp: 1829,
          events: [
            {
              type: "shot",
              text: ["Toet!", "Nog 70 shotjes"],
              shotCount: 30,
            },
          ],
        },
        {
          timestamp: 1858,
          events: [
            {
              type: "song",
              text: ["Basto!", "Again and Again"],
            },
          ],
        },
        {
          timestamp: 1887,
          events: [
            {
              type: "shot",
              text: ["Over de dertig!", "Nog 69 (nice) shotjes"],
              shotCount: 31,
            },
          ],
        },
        {
          timestamp: 1916,
          events: [
            {
              type: "song",
              text: ["David Guetta feat. Sia", "Titanium"],
            },
          ],
        },
        {
          timestamp: 1945,
          events: [
            {
              type: "shot",
              text: ["Trek een ad!", "Nog 68 shotjes"],
              shotCount: 32,
            },
          ],
        },
        {
          timestamp: 1959,
          events: [
            {
              type: "song",
              text: ["Gala", "Freed From Desire"],
            },
          ],
        },
        {
          timestamp: 2004,
          events: [
            {
              type: "shot",
              text: ["Nummertje 33!", "Nog 67 shotjes!"],
              shotCount: 33,
            },
          ],
        },
        {
          timestamp: 2034,
          events: [
            {
              type: "song",
              text: ["Wolter Kroes", "Viva Hollandia"],
            },
          ],
        },
        {
          timestamp: 2064,
          events: [
            {
              type: "shot",
              text: ["Voor het vaderland!", "Nog 66 shotjes"],
              shotCount: 34,
            },
          ],
        },
        {
          timestamp: 2090,
          events: [
            {
              type: "song",
              text: ["Spongebob Squarepants", "Het Spongebob Squarepants-lied"],
            },
          ],
        },
        {
          timestamp: 2125,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 65 shotjes"],
              shotCount: 35,
            },
          ],
        },
        {
          timestamp: 2128,
          events: [
            {
              type: "song",
              text: ["Westlife", "Uptown Girl"],
            },
          ],
        },
        {
          timestamp: 2179,
          events: [
            {
              type: "song",
              text: ["Aqua", "Barbie Girl"],
            },
          ],
        },
        {
          timestamp: 2194,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 64 shotjes"],
              shotCount: 36,
            },
          ],
        },
        {
          timestamp: 2225,
          events: [
            {
              type: "song",
              text: ["Guillermo & Tropical Danny", "Toppertje"],
            },
          ],
        },
        {
          timestamp: 2245,
          events: [
            {
              type: "shot",
              text: ["In dat keelgaatje!", "Nog 63 shotjes"],
              shotCount: 37,
            },
          ],
        },
        {
          timestamp: 2305,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 62 shotjes"],
              shotCount: 38,
            },
            {
              type: "song",
              text: ["The Bloody Beetroots feat. Steve Aoki", "Warp 1.9"],
            },
          ],
        },
        {
          timestamp: 2374,
          events: [
            {
              type: "shot",
              text: ["Toet!", "Nog 61 shotjes"],
              shotCount: 39,
            },
            {
              type: "song",
              text: ["David Guetta & Showtek feat. Vassy", "Bad"],
            },
          ],
        },
        {
          timestamp: 2431,
          events: [
            {
              type: "shot",
              text: ["40 alweer!", "Nog 60 shotjes"],
              shotCount: 40,
            },
            {
              type: "song",
              text: ["Showtek & Justin Prime", "Cannonball"],
            },
          ],
        },
        {
          timestamp: 2444,
          events: [
            {
              type: "time",
              text: ["Nog 60 minuten!", "Een klein uurtje"],
            },
          ],
        },
        {
          timestamp: 2460,
          events: [
            {
              type: "song",
              text: ["Die Atzen", "Disco Pogo"],
            },
          ],
        },
        {
          timestamp: 2489,
          events: [
            {
              type: "shot",
              text: ["Zuipen!", "Nog 59 shotjes"],
              shotCount: 41,
            },
          ],
        },
        {
          timestamp: 2534,
          events: [
            {
              type: "song",
              text: ["Lorenz Büffel", "Johnny Däpp"],
            },
          ],
        },
        {
          timestamp: 2555,
          events: [
            {
              type: "shot",
              text: ["Dab dat glas naar je mond!", "Nog 58 shotjes"],
              shotCount: 42,
            },
          ],
        },
        {
          timestamp: 2587,
          events: [
            {
              type: "song",
              text: ["Zware Jongens", "Jodeljump"],
            },
          ],
        },
        {
          timestamp: 2606,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 57 shotjes"],
              shotCount: 43,
            },
          ],
        },
        {
          timestamp: 2635,
          events: [
            {
              type: "song",
              text: ["Parla & Pardoux", "Liberté"],
            },
          ],
        },
        {
          timestamp: 2664,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 56 shotjes"],
              shotCount: 44,
            },
          ],
        },
        {
          timestamp: 2695,
          events: [
            {
              type: "song",
              text: ["Markus Becker", "Das rote Pferd"],
            },
          ],
        },
        {
          timestamp: 2729,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 55 shotjes"],
              shotCount: 45,
            },
          ],
        },
        {
          timestamp: 2743,
          events: [
            {
              type: "song",
              text: ["Olaf Henning", "Cowboy und Indianer"],
            },
          ],
        },
        {
          timestamp: 2784,
          events: [
            {
              type: "shot",
              text: ["Adje!", "Nog 54 shotjes"],
              shotCount: 46,
            },
          ],
        },
        {
          timestamp: 2785,
          events: [
            {
              type: "song",
              text: ["Ch!pz", "Cowboy"],
            },
          ],
        },
        {
          timestamp: 2824,
          events: [
            {
              type: "song",
              text: ["Toy-Box", "Tarzan & Jane"],
            },
          ],
        },
        {
          timestamp: 2850,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 53 shotjes"],
              shotCount: 47,
            },
          ],
        },
        {
          timestamp: 2879,
          events: [
            {
              type: "song",
              text: ["Toy-Box", "Sailor Song"],
            },
          ],
        },
        {
          timestamp: 2919,
          events: [
            {
              type: "shot",
              text: ["Bijna op de helft!", "Nog 52 shotjes"],
              shotCount: 48,
            },
          ],
        },
        {
          timestamp: 2922,
          events: [
            {
              type: "song",
              text: ["Vengaboys", "Boom, Boom, Boom, Boom!!"],
            },
          ],
        },
        {
          timestamp: 2975,
          events: [
            {
              type: "shot",
              text: ["Bam!", "Nog 51 shotjes"],
              shotCount: 49,
            },
          ],
        },
        {
          timestamp: 2998,
          events: [
            {
              type: "song",
              text: ["Vengaboys", "To Brazil!"],
            },
          ],
        },
        {
          timestamp: 3024,
          events: [
            {
              type: "time",
              text: ["Nog 50 minuten!", "We zijn op de helft!"],
            },
          ],
        },
        {
          timestamp: 3039,
          events: [
            {
              type: "shot",
              text: ["Abraham!", "Nog 50 shotjes"],
              shotCount: 50,
            },
          ],
        },
        {
          timestamp: 3070,
          events: [
            {
              type: "song",
              text: ["Snollebollekes", "Bam bam (bam)"],
            },
          ],
        },
        {
          timestamp: 3104,
          events: [
            {
              type: "shot",
              text: ["Tweede helft!", "Nog 49 shotjes"],
              shotCount: 51,
            },
          ],
        },
        {
          timestamp: 3120,
          events: [
            {
              type: "song",
              text: ["Def Rhymz", "Schudden"],
            },
          ],
        },
        {
          timestamp: 3160,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 48 shotjes"],
              shotCount: 52,
            },
          ],
        },
        {
          timestamp: 3190,
          events: [
            {
              type: "song",
              text: ["Cooldown Café", "Hey baby"],
            },
          ],
        },
        {
          timestamp: 3215,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 47 shotjes"],
              shotCount: 53,
            },
          ],
        },
        {
          timestamp: 3232,
          events: [
            {
              type: "song",
              text: ["Gebroeders Ko", "Schatje, mag ik je foto"],
            },
          ],
        },
        {
          timestamp: 3279,
          events: [
            {
              type: "shot",
              text: ["Zuipen!", "Nog 46 shotjes"],
              shotCount: 54,
            },
            {
              type: "song",
              text: ["Guus Meeuwis", "Het is een nacht"],
            },
          ],
        },
        {
          timestamp: 3341,
          events: [
            {
              type: "shot",
              text: ["Adje!", "Nog 45 shotjes"],
              shotCount: 55,
            },
          ],
        },
        {
          timestamp: 3356,
          events: [
            {
              type: "song",
              text: ["Tom Waes", "Dos cervezas"],
            },
          ],
        },
        {
          timestamp: 3398,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 44 shotjes"],
              shotCount: 56,
            },
          ],
        },
        {
          timestamp: 3412,
          events: [
            {
              type: "song",
              text: ["Peter Wackel", "Vollgas"],
            },
          ],
        },
        {
          timestamp: 3439,
          events: [
            {
              type: "song",
              text: ["Peter Wackel", "Scheiß drauf!"],
            },
          ],
        },
        {
          timestamp: 3465,
          events: [
            {
              type: "shot",
              text: ["Hard gaan!", "Nog 43 shotjes"],
              shotCount: 57,
            },
          ],
        },
        {
          timestamp: 3467,
          events: [
            {
              type: "song",
              text: ["Ikke Hüftgold", "Dicke titten, kartoffelsalat"],
            },
          ],
        },
        {
          timestamp: 3520,
          events: [
            {
              type: "shot",
              text: ["Hap, slok, weg!", "Nog 42 shotjes"],
              shotCount: 58,
            },
          ],
        },
        {
          timestamp: 3521,
          events: [
            {
              type: "song",
              text: ["Tim Toupet", "Fliegerlied (So ein schöner Tag)"],
            },
          ],
        },
        {
          timestamp: 3564,
          events: [
            {
              type: "song",
              text: ["Cooldown Café", "Met z'n allen"],
            },
          ],
        },
        {
          timestamp: 3577,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 41 shotjes"],
              shotCount: 59,
            },
          ],
        },
        {
          timestamp: 3636,
          events: [
            {
              type: "time",
              text: ["Nog 40 minuten!", "Uurtje achter de rug"],
            },
          ],
        },
        {
          timestamp: 3643,
          events: [
            {
              type: "shot",
              text: ["Glas 60!", "Nog 40 shotjes"],
              shotCount: 60,
            },
          ],
        },
        {
          timestamp: 3659,
          events: [
            {
              type: "song",
              text: [
                "The Partysquad feat. Jayh, Sjaak & Reverse",
                "Helemaal naar de klote",
              ],
            },
          ],
        },
        {
          timestamp: 3687,
          events: [
            {
              type: "song",
              text: ["K-Liber", "Viben"],
            },
          ],
        },
        {
          timestamp: 3700,
          events: [
            {
              type: "shot",
              text: ["Adje!", "Nog 39 shotjes"],
              shotCount: 61,
            },
          ],
        },
        {
          timestamp: 3753,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 38 shotjes"],
              shotCount: 62,
            },
          ],
        },
        {
          timestamp: 3755,
          events: [
            {
              type: "song",
              text: [
                "FeestDJRuud & Dirtcaps feat. Sjaak & Kraantje Pappie",
                "Weekend",
              ],
            },
          ],
        },
        {
          timestamp: 3808,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 37 shotjes"],
              shotCount: 63,
            },
            {
              type: "song",
              text: ["Lawineboys", "Joost"],
            },
          ],
        },
        {
          timestamp: 3860,
          events: [
            {
              type: "song",
              text: ["Gebroeders Ko", "Ik heb een toeter op mijn waterscooter"],
            },
          ],
        },
        {
          timestamp: 3874,
          events: [
            {
              type: "shot",
              text: ["Toe-toe-toeter!", "Nog 36 shotjes"],
              shotCount: 64,
            },
          ],
        },
        {
          timestamp: 3904,
          events: [
            {
              type: "song",
              text: ["Gebroeders Ko", "Tringeling"],
            },
          ],
        },
        {
          timestamp: 3931,
          events: [
            {
              type: "shot",
              text: ["Tringeling!", "Nog 35 shotjes"],
              shotCount: 65,
            },
          ],
        },
        {
          timestamp: 3975,
          events: [
            {
              type: "song",
              text: ["Basshunter", "Boten Anna"],
            },
          ],
        },
        {
          timestamp: 3988,
          events: [
            {
              type: "shot",
              text: ["Zuipen!", "Nog 34 shotjes"],
              shotCount: 66,
            },
          ],
        },
        {
          timestamp: 4029,
          events: [
            {
              type: "song",
              text: ["Lawineboys", "Wat zullen we drinken"],
            },
          ],
        },
        {
          timestamp: 4050,
          events: [
            {
              type: "shot",
              text: ["Dorst!", "Nog 33 shotjes"],
              shotCount: 67,
            },
          ],
        },
        {
          timestamp: 4081,
          events: [
            {
              type: "song",
              text: ["Lamme Frans", "Wakker met een biertje!"],
            },
          ],
        },
        {
          timestamp: 4113,
          events: [
            {
              type: "shot",
              text: ["Biertje in de hand...", "Nog 32 shotjes"],
              shotCount: 68,
            },
          ],
        },
        {
          timestamp: 4124.18,
          events: [
            {
              type: "song",
              text: ["Lawineboys feat. DJ Jerome", "Seks met die kale"],
            },
          ],
        },
        {
          timestamp: 4175,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 31 shotjes"],
              shotCount: 69,
            },
          ],
        },
        {
          timestamp: 4177,
          events: [
            {
              type: "song",
              text: ["Zombie Nation", "Kernkraft 400"],
            },
          ],
        },
        {
          timestamp: 4214,
          events: [
            {
              type: "time",
              text: ["Nog 30 minuten!", "Half uurtje nog maar"],
            },
          ],
        },
        {
          timestamp: 4228,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 30 shotjes"],
              shotCount: 70,
            },
          ],
        },
        {
          timestamp: 4230,
          events: [
            {
              type: "song",
              text: ["DJ Boozywoozy", "Party Affair"],
            },
          ],
        },
        {
          timestamp: 4280,
          events: [
            {
              type: "shot",
              text: ["Tik maar achterover!", "Nog 29 shotjes"],
              shotCount: 71,
            },
          ],
        },
        {
          timestamp: 4289,
          events: [
            {
              type: "song",
              text: ["2Unlimited", "No Limit"],
            },
          ],
        },
        {
          timestamp: 4341,
          events: [
            {
              type: "shot",
              text: ["Alcoholic party!", "Nog 28 shotjes"],
              shotCount: 72,
            },
          ],
        },
        {
          timestamp: 4343,
          events: [
            {
              type: "song",
              text: ["DJ Kicken vs. MC-Q", "Ain't No Party"],
            },
          ],
        },
        {
          timestamp: 4408,
          events: [
            {
              type: "shot",
              text: ["Toet!", "Nog 27 shotjes"],
              shotCount: 73,
            },
          ],
        },
        {
          timestamp: 4411,
          events: [
            {
              type: "song",
              text: ["Jan Wayne", "Because the Night"],
            },
          ],
        },
        {
          timestamp: 4455,
          events: [
            {
              type: "song",
              text: ["Cascada", "Everytime We Touch"],
            },
          ],
        },
        {
          timestamp: 4467,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 26 shotjes"],
              shotCount: 74,
            },
          ],
        },
        {
          timestamp: 4510,
          events: [
            {
              type: "song",
              text: ["Gigi D'Agostino", "L'amour toujours"],
            },
          ],
        },
        {
          timestamp: 4521,
          events: [
            {
              type: "shot",
              text: ["Shotje 75!", "Nog 25 shotjes"],
              shotCount: 75,
            },
          ],
        },
        {
          timestamp: 4573,
          events: [
            {
              type: "song",
              text: ["Jason Paige", "Gotta Catch 'M All"],
            },
          ],
        },
        {
          timestamp: 4578,
          events: [
            {
              type: "shot",
              text: ["Trek een ad!", "Nog 24 shotjes"],
              shotCount: 76,
            },
          ],
        },
        {
          timestamp: 4636,
          events: [
            {
              type: "song",
              text: ["Scooter", "How Much Is The Fish"],
            },
          ],
        },
        {
          timestamp: 4645,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 23 shotjes"],
              shotCount: 77,
            },
          ],
        },
        {
          timestamp: 4674,
          events: [
            {
              type: "song",
              text: ["Scooter", "Weekend"],
            },
          ],
        },
        {
          timestamp: 4701,
          events: [
            {
              type: "shot",
              text: ["scToeter!", "Nog 22 shotjes"],
              shotCount: 78,
            },
          ],
        },
        {
          timestamp: 4714,
          events: [
            {
              type: "song",
              text: ["Scooter", "One (Always Hardcore)"],
            },
          ],
        },
        {
          timestamp: 4764,
          events: [
            {
              type: "shot",
              text: ["En door!", "Nog 21 shotjes"],
              shotCount: 79,
            },
          ],
        },
        {
          timestamp: 4766,
          events: [
            {
              type: "song",
              text: ["Scooter", "Maria (I Like It Loud)"],
            },
          ],
        },
        {
          timestamp: 4815,
          events: [
            {
              type: "time",
              text: ["Nog 20 minuten!", "Geef alles, behalve over"],
            },
          ],
        },
        {
          timestamp: 4818,
          events: [
            {
              type: "song",
              text: ["Scooter", "J'adore Hardcore"],
            },
          ],
        },
        {
          timestamp: 4829,
          events: [
            {
              type: "shot",
              text: ["Al 80 in de mik!", "Nog 20 shotjes"],
              shotCount: 80,
            },
          ],
        },
        {
          timestamp: 4860,
          events: [
            {
              type: "song",
              text: ["Wildstylez feat. Niels Geusebroek", "Year of Summer"],
            },
          ],
        },
        {
          timestamp: 4888,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 19 shotjes"],
              shotCount: 81,
            },
          ],
        },
        {
          timestamp: 4929,
          events: [
            {
              type: "song",
              text: ["Brennan Heart & Wildstylez", "Lose My Mind"],
            },
          ],
        },
        {
          timestamp: 4953,
          events: [
            {
              type: "shot",
              text: ["Bakken vouwen!", "Nog 18 shotjes"],
              shotCount: 82,
            },
          ],
        },
        {
          timestamp: 5004,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 17 shotjes"],
              shotCount: 83,
            },
          ],
        },
        {
          timestamp: 5006,
          events: [
            {
              type: "song",
              text: ["Starkoo", "Ik wil je"],
            },
          ],
        },
        {
          timestamp: 5059,
          events: [
            {
              type: "song",
              text: ["Feestteam", "Let It Be / Hey Jude (mix)"],
            },
          ],
        },
        {
          timestamp: 5071,
          events: [
            {
              type: "shot",
              text: ["Let it bier!", "Nog 16 shotjes"],
              shotCount: 84,
            },
          ],
        },
        {
          timestamp: 5125,
          events: [
            {
              type: "shot",
              text: ["Adje numero 85!", "Nog 15 shotjes"],
              shotCount: 85,
            },
          ],
        },
        {
          timestamp: 5179,
          events: [
            {
              type: "song",
              text: ["DJ Nikolai & DJ Mike van Dijk", "Piano Man"],
            },
          ],
        },
        {
          timestamp: 5188.35,
          events: [
            {
              type: "shot",
              text: ["Toet!", "Nog 14 shotjes"],
              shotCount: 86,
            },
          ],
        },
        {
          timestamp: 5190,
          events: [
            {
              type: "talk",
              text: ["Gewoon doorgaan!", "Deze Piano Man telt niet"],
            },
          ],
        },
        {
          timestamp: 5237,
          events: [
            {
              type: "song",
              text: ["Robbie Williams", "Angels"],
            },
          ],
        },
        {
          timestamp: 5243,
          events: [
            {
              type: "shot",
              text: ["Drinken!", "Nog 13 shotjes"],
              shotCount: 87,
            },
          ],
        },
        {
          timestamp: 5278,
          events: [
            {
              type: "song",
              text: ["Enrique Iglesias", "Hero"],
            },
          ],
        },
        {
          timestamp: 5309,
          events: [
            {
              type: "shot",
              text: ["I can't be your biero!", "Nog 12 shotjes"],
              shotCount: 88,
            },
          ],
        },
        {
          timestamp: 5331,
          events: [
            {
              type: "song",
              text: ["Whitney Houston", "I Will Always Love You"],
            },
          ],
        },
        {
          timestamp: 5374,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 11 shotjes"],
              shotCount: 89,
            },
          ],
        },
        {
          timestamp: 5380,
          events: [
            {
              type: "song",
              text: ["Mariah Carey", "All I Want For Christmas"],
            },
          ],
        },
        {
          timestamp: 5431,
          events: [
            {
              type: "shot",
              text: ["90 in de keel, hierna niet veel!", "Nog 10 shotjes"],
              shotCount: 90,
            },
          ],
        },
        {
          timestamp: 5441,
          events: [
            {
              type: "time",
              text: ["Nog 10 minuten!", "De laatste loodjes"],
            },
          ],
        },
        {
          timestamp: 5444,
          events: [
            {
              type: "song",
              text: ["Kraantje Pappie", "Feesttent (FeestDJRuud remix)"],
            },
          ],
        },
        {
          timestamp: 5488,
          events: [
            {
              type: "shot",
              text: ["Zuip je uit de dubbele cijfers!", "Nog 9 shotjes"],
              shotCount: 91,
            },
          ],
        },
        {
          timestamp: 5490.25,
          events: [
            {
              type: "song",
              text: ["New Kids feat. DJ Paul Elstak", "Turbo"],
            },
          ],
        },
        {
          timestamp: 5556,
          events: [
            {
              type: "shot",
              text: ["Zuipen!", "Nog 8 shotjes"],
              shotCount: 92,
            },
          ],
        },
        {
          timestamp: 5558,
          events: [
            {
              type: "song",
              text: ["Lipstick", "I'm a Raver"],
            },
          ],
        },
        {
          timestamp: 5591,
          events: [
            {
              type: "song",
              text: ["Nakatomi", "Children of the Night"],
            },
          ],
        },
        {
          timestamp: 5612,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 7 shotjes"],
              shotCount: 93,
            },
          ],
        },
        {
          timestamp: 5614,
          events: [
            {
              type: "song",
              text: ["Charly Lownoise & Mental Theo", "Wonderful Days"],
            },
          ],
        },
        {
          timestamp: 5659,
          events: [
            {
              type: "song",
              text: ["DJ Paul Elstak", "Luv You More"],
            },
          ],
        },
        {
          timestamp: 5667,
          events: [
            {
              type: "shot",
              text: ["Bijna daar!", "Nog 6 shotjes"],
              shotCount: 94,
            },
          ],
        },
        {
          timestamp: 5704,
          events: [
            {
              type: "song",
              text: ["DJ Paul Elstak", "Rainbow In The Sky"],
            },
          ],
        },
        {
          timestamp: 5716,
          events: [
            {
              type: "time",
              text: ["Nog 5 minuten!", "Zet de shotjes maar klaar"],
            },
          ],
        },
        {
          timestamp: 5731.46,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 5 shotjes"],
              shotCount: 95,
            },
          ],
        },
        {
          timestamp: 5733,
          events: [
            {
              type: "song",
              text: ["Evil Activities", "Nobody Said It Was Easy"],
            },
          ],
        },
        {
          timestamp: 5783,
          events: [
            {
              type: "shot",
              text: ["Slok 96!", "Nog 4 shotjes"],
              shotCount: 96,
            },
          ],
        },
        {
          timestamp: 5827,
          events: [
            {
              type: "song",
              text: ["Melrose", "O"],
            },
          ],
        },
        {
          timestamp: 5846,
          events: [
            {
              type: "shot",
              text: ["Voor de 97e keer!", "Nog 3 shotjes"],
              shotCount: 97,
            },
          ],
        },
        {
          timestamp: 5855,
          events: [
            {
              type: "song",
              text: ["Backstreet Boys", "I Want It That Way"],
            },
          ],
        },
        {
          timestamp: 5902,
          events: [
            {
              type: "shot",
              text: ["Nummer 98!", "Nog 2 shotjes"],
              shotCount: 98,
            },
          ],
        },
        {
          timestamp: 5933,
          events: [
            {
              type: "song",
              text: ["R. Kelly", "The World's Greatest"],
            },
          ],
        },
        {
          timestamp: 5970,
          events: [
            {
              type: "shot",
              text: ["Nummer 99!", "Nog 1 shotje"],
              shotCount: 99,
            },
            {
              type: "song",
              text: ["Céline Dion", "My Heart Will Go On"],
            },
          ],
        },
        {
          timestamp: 6020,
          events: [
            {
              type: "shot",
              text: ["CENTURION!", "Geen shots meer"],
              shotCount: 100,
            },
          ],
        },
      ],
    },
    {
      name: "Centurion 2.0",
      songFile: "songs/centurion2.m4a",
      feed: [
        {
          timestamp: 0,
          events: [
            {
              type: "talk",
              text: [
                "Zet je schrap!",
                "We gaan zo beginnen aan Centurion 2.0 !",
              ],
            },
          ],
        },
        {
          timestamp: 124,
          events: [
            {
              type: "shot",
              text: ["De eerste!", "Nog 99 shotjes"],
              shotCount: 1,
            },
            {
              type: "song",
              text: ["Lou Bega", "Mambo no. 5"],
            },
          ],
        },
        {
          timestamp: 188,
          events: [
            {
              type: "shot",
              text: ["Nummertje twee!", "Nog 98 shotjes"],
              shotCount: 2,
            },
            {
              type: "song",
              text: ["Monique smit", "Wild"],
            },
          ],
        },
        {
          timestamp: 257.56,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 97 shotjes"],
              shotCount: 3,
            },
            {
              type: "song",
              text: ["Britney spears", "...Baby one more time"],
            },
          ],
        },
        {
          timestamp: 308.81,
          events: [
            {
              type: "shot",
              text: ["Niet gooien!", "Nog 96 shotjes"],
              shotCount: 4,
            },
            {
              type: "song",
              text: ["Nielson", "Sexy als ik dans"],
            },
          ],
        },
        {
          timestamp: 362,
          events: [
            {
              type: "song",
              text: ["Luis Fonsi ft. Daddy Yankee", "Despacito"],
            },
          ],
        },
        {
          timestamp: 369.86,
          events: [
            {
              type: "shot",
              text: ["Lustrum!", "Nog 95 shotjes"],
              shotCount: 5,
            },
          ],
        },
        {
          timestamp: 421,
          events: [
            {
              type: "song",
              text: ["K3", "Heyah mama"],
            },
          ],
        },
        {
          timestamp: 429.52,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 94 shotjes"],
              shotCount: 6,
            },
          ],
        },
        {
          timestamp: 476,
          events: [
            {
              type: "song",
              text: ["K3", "Heyah mama"],
            },
          ],
        },
        {
          timestamp: 490,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 93 shotjes"],
              shotCount: 7,
            },
            {
              type: "song",
              text: ["Jan smit", "Als de morgen is gekomen"],
            },
          ],
        },
        {
          timestamp: 549.45,
          events: [
            {
              type: "shot",
              text: ["Trek een ad!", "Nog 92 shotjes"],
              shotCount: 8,
            },
            {
              type: "song",
              text: ["Beyonce", "Single ladies"],
            },
          ],
        },
        {
          timestamp: 590,
          events: [
            {
              type: "song",
              text: ["Dio", "Tijdmachine"],
            },
          ],
        },
        {
          timestamp: 615.51,
          events: [
            {
              type: "shot",
              text: ["We zijn er nog lang niet!", "Nog 91 shotjes"],
              shotCount: 9,
            },
          ],
        },
        {
          timestamp: 639,
          events: [
            {
              type: "song",
              text: ["Alphabeat", "Fascination"],
            },
          ],
        },
        {
          timestamp: 673.85,
          events: [
            {
              type: "shot",
              text: ["Nummer tien!", "Nog 90 shotjes"],
              shotCount: 10,
            },
          ],
        },
        {
          timestamp: 693,
          events: [
            {
              type: "song",
              text: ["Sterrenstof", "De Jeugd van Tegenwoordig"],
            },
          ],
        },
        {
          timestamp: 731,
          events: [
            {
              type: "shot",
              text: ["Met vriendelijke toet!", "Nog 89 shotjes"],
              shotCount: 11,
            },
          ],
        },
        {
          timestamp: 780,
          events: [
            {
              type: "song",
              text: ["Lynyrd Skynyrd", "Sweet Home Alabama"],
            },
            {
              type: "song",
              text: ["Lionel Richie", "All night long"],
            },
          ],
        },
        {
          timestamp: 787.36,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 88 shotjes"],
              shotCount: 12,
            },
          ],
        },
        {
          timestamp: 846,
          events: [
            {
              type: "song",
              text: ["T-Spoon", "Sex on the beach"],
            },
          ],
        },
        {
          timestamp: 853.6,
          events: [
            {
              type: "shot",
              text: ["Ongeluksshotje 13!", "Nog 87 shotjes"],
              shotCount: 13,
            },
          ],
        },
        {
          timestamp: 901,
          events: [
            {
              type: "song",
              text: ["Ultimate Kaos", "Casanova"],
            },
          ],
        },
        {
          timestamp: 921.91,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 86 shotjes"],
              shotCount: 14,
            },
          ],
        },
        {
          timestamp: 951,
          events: [
            {
              type: "song",
              text: ["Opus", "Live is life"],
            },
          ],
        },
        {
          timestamp: 976.1,
          events: [
            {
              type: "shot",
              text: ["Fünfzehn!", "Nog 85 shotjes"],
              shotCount: 15,
            },
          ],
        },
        {
          timestamp: 1000,
          events: [
            {
              type: "song",
              text: ["Nick en Simon", "Rosanne"],
            },
          ],
        },
        {
          timestamp: 1038.14,
          events: [
            {
              type: "shot",
              text: ["Toet!", "Nog 84 shotjes"],
              shotCount: 16,
            },
            {
              type: "song",
              text: ["Vengaboys", "We're going to ibiza"],
            },
          ],
        },
        {
          timestamp: 1068,
          events: [
            {
              type: "song",
              text: ["K3", "10000 luchtballonnen"],
            },
          ],
        },
        {
          timestamp: 1093.12,
          events: [
            {
              type: "shot",
              text: ["Zuipen!", "Nog 83 shotjes"],
              shotCount: 17,
            },
          ],
        },
        {
          timestamp: 1114,
          events: [
            {
              type: "song",
              text: ["Bizzey", "Traag"],
            },
          ],
        },
        {
          timestamp: 1157.05,
          events: [
            {
              type: "shot",
              text: ["18, legaal!", "Nog 82 shotjes"],
              shotCount: 18,
            },
            {
              type: "song",
              text: ["Natasha Bedingfield", "Unwritten"],
            },
          ],
        },
        {
          timestamp: 1196,
          events: [
            {
              type: "song",
              text: ["Sean Paul", "Trumpets"],
            },
          ],
        },
        {
          timestamp: 1213.93,
          events: [
            {
              type: "shot",
              text: ["Laatste shotje als tiener!", "Nog 81 shotjes"],
              shotCount: 19,
            },
          ],
        },
        {
          timestamp: 1251,
          events: [
            {
              type: "song",
              text: ["Backstreet boys", "Everybody"],
            },
          ],
        },
        {
          timestamp: 1268.35,
          events: [
            {
              type: "shot",
              text: ["Adje!", "Nog 80 shotjes"],
              shotCount: 20,
            },
          ],
        },
        {
          timestamp: 1314,
          events: [
            {
              type: "song",
              text: ["Survivor", "Eye of the tiger"],
            },
          ],
        },
        {
          timestamp: 1329.11,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 79 shotjes"],
              shotCount: 21,
            },
          ],
        },
        {
          timestamp: 1368,
          events: [
            {
              type: "song",
              text: ["Hansson", "MMMBop"],
            },
          ],
        },
        {
          timestamp: 1391.36,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 78 shotjes"],
              shotCount: 22,
            },
            {
              type: "song",
              text: ["Katrina & The Waves", "Walking on Sunshine"],
            },
          ],
        },
        {
          timestamp: 1425,
          events: [
            {
              type: "song",
              text: ["Frans Bauer", "Heb je even voor mij"],
            },
          ],
        },
        {
          timestamp: 1454.53,
          events: [
            {
              type: "shot",
              text: ["Claxon!", "Nog 77 shotjes"],
              shotCount: 23,
            },
          ],
        },
        {
          timestamp: 1494,
          events: [
            {
              type: "song",
              text: ["Monique Smit", "Blijf je vanvond"],
            },
          ],
        },
        {
          timestamp: 1514.75,
          events: [
            {
              type: "shot",
              text: ["En nu even op standje maximaal!", "Nog 76 shotjes"],
              shotCount: 24,
            },
          ],
        },
        {
          timestamp: 1570.23,
          events: [
            {
              type: "shot",
              text: ["Halve Abraham!", "Nog 75 shotjes"],
              shotCount: 25,
            },
            {
              type: "song",
              text: ["John De Bever", "Lach van mijn gezicht"],
            },
          ],
        },
        {
          timestamp: 1607,
          events: [
            {
              type: "song",
              text: ["Bloem", "Even aan mijn moeder vragen"],
            },
          ],
        },
        {
          timestamp: 1630.85,
          events: [
            {
              type: "shot",
              text: ["Hoch die Hände!", "Nog 74 shotjes"],
              shotCount: 26,
            },
            {
              type: "song",
              text: ["Whitney Houston", "I wanna dance with somebody"],
            },
          ],
        },
        {
          timestamp: 1688.2,
          events: [
            {
              type: "shot",
              text: ["Voel je 'm al?", "Nog 73 shotjes"],
              shotCount: 27,
            },
          ],
        },
        {
          timestamp: 1707,
          events: [
            {
              type: "song",
              text: ["Christina Aquilera", "Carwash"],
            },
            {
              type: "song",
              text: ["Carglass", "Jingle"],
            },
          ],
        },
        {
          timestamp: 1751.13,
          events: [
            {
              type: "shot",
              text: ["Project X!", "Nog 72 shotjes"],
              shotCount: 28,
            },
            {
              type: "song",
              text: ["Kool & the gang", "Celebration"],
            },
          ],
        },
        {
          timestamp: 1815.66,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 71 shotjes"],
              shotCount: 29,
            },
            {
              type: "song",
              text: ["Pitbull", "Fireball"],
            },
          ],
        },
        {
          timestamp: 1870.16,
          events: [
            {
              type: "shot",
              text: ["Toet!", "Nog 70 shotjes"],
              shotCount: 30,
            },
          ],
        },
        {
          timestamp: 1903,
          events: [
            {
              type: "song",
              text: ["Kungs & cookin", "This girl"],
            },
          ],
        },
        {
          timestamp: 1926.31,
          events: [
            {
              type: "shot",
              text: ["Over de dertig!", "Nog 69 (hehe) shotjes"],
              shotCount: 31,
            },
            {
              type: "song",
              text: ["DJ GiaN", "Soundbox mix vol. 04"],
            },
          ],
        },
        {
          timestamp: 1970,
          events: [
            {
              type: "song",
              text: ["Carly Rae Jepsen", "Call me maybe"],
            },
          ],
        },
        {
          timestamp: 1991.94,
          events: [
            {
              type: "shot",
              text: ["Trek een ad!", "Nog 68 shotjes"],
              shotCount: 32,
            },
          ],
        },
        {
          timestamp: 2022,
          events: [
            {
              type: "song",
              text: ["DJ Antoine", "Welcome to st. tropez"],
            },
          ],
        },
        {
          timestamp: 2053.52,
          events: [
            {
              type: "shot",
              text: ["Nummertje 33!", "Nog 67 shotjes!"],
              shotCount: 33,
            },
          ],
        },
        {
          timestamp: 2073,
          events: [
            {
              type: "song",
              text: ["Jamai", "Step right up"],
            },
          ],
        },
        {
          timestamp: 2112.71,
          events: [
            {
              type: "shot",
              text: ["Voor het vaderland!", "Nog 66 shotjes"],
              shotCount: 34,
            },
          ],
        },
        {
          timestamp: 2132,
          events: [
            {
              type: "song",
              text: ["DJ Sjaak", "Stap voor stap"],
            },
          ],
        },
        {
          timestamp: 2172.92,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 65 shotjes"],
              shotCount: 35,
            },
            {
              type: "song",
              text: ["Beyonce", "Run the world"],
            },
          ],
        },
        {
          timestamp: 2198,
          events: [
            {
              type: "song",
              text: ["Europe", "Final Countdown"],
            },
          ],
        },
        {
          timestamp: 2238.59,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 64 shotjes"],
              shotCount: 36,
            },
          ],
        },
        {
          timestamp: 2265,
          events: [
            {
              type: "song",
              text: ["Gloria Gaynor", "I will survive"],
            },
          ],
        },
        {
          timestamp: 2288.39,
          events: [
            {
              type: "shot",
              text: ["In dat keelgaatje!", "Nog 63 shotjes"],
              shotCount: 37,
            },
            {
              type: "song",
              text: ["Helmes house band", "I will survive"],
            },
          ],
        },
        {
          timestamp: 2347.47,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 62 shotjes"],
              shotCount: 38,
            },
            {
              type: "song",
              text: ["Marianne Rosenberg", "Ich bin wie du"],
            },
          ],
        },
        {
          timestamp: 2332,
          events: [
            {
              type: "song",
              text: ["Marco Borsato", "Dromen zijn bedrog"],
            },
          ],
        },
        {
          timestamp: 2417.34,
          events: [
            {
              type: "shot",
              text: ["Toet!", "Nog 61 shotjes"],
              shotCount: 39,
            },
          ],
        },
        {
          timestamp: 2448,
          events: [
            {
              type: "song",
              text: ["DJ Otzi", "Anton aus tirol"],
            },
          ],
        },
        {
          timestamp: 2473.4,
          events: [
            {
              type: "shot",
              text: ["40 alweer!", "Nog 60 shotjes"],
              shotCount: 40,
            },
          ],
        },
        {
          timestamp: 2490,
          events: [
            {
              type: "song",
              text: ["Tim Toupet", "Tote Enten"],
            },
          ],
        },
        {
          timestamp: 2519,
          events: [
            {
              type: "song",
              text: ["Neil Diamond", "Sweet caroline"],
            },
          ],
        },
        {
          timestamp: 2537.2,
          events: [
            {
              type: "shot",
              text: ["Zuipen!", "Nog 59 shotjes"],
              shotCount: 41,
            },
          ],
        },
        {
          timestamp: 2560,
          events: [
            {
              type: "song",
              text: ["Flo Rida", "Right Round"],
            },
          ],
        },
        {
          timestamp: 2589.25,
          events: [
            {
              type: "shot",
              text: ["Dab dat glas naar je mond!", "Nog 58 shotjes"],
              shotCount: 42,
            },
            {
              type: "song",
              text: ["AronChupa", "I'm an Albatraoz"],
            },
          ],
        },
        {
          timestamp: 2646.81,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 57 shotjes"],
              shotCount: 43,
            },
            {
              type: "song",
              text: ["K3", "Leonardi Di Caprio"],
            },
          ],
        },
        {
          timestamp: 2687,
          events: [
            {
              type: "song",
              text: ["Marrtin selveig", "Hello"],
            },
          ],
        },
        {
          timestamp: 2715.46,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 56 shotjes"],
              shotCount: 44,
            },
          ],
        },
        {
          timestamp: 2745,
          events: [
            {
              type: "song",
              text: ["The Black Eyes Peas", "I Gotta Feeling"],
            },
          ],
        },
        {
          timestamp: 2772.92,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 55 shotjes"],
              shotCount: 45,
            },
          ],
        },
        {
          timestamp: 2819,
          events: [
            {
              type: "song",
              text: ["Temperature", "Sean Paul"],
            },
          ],
        },
        {
          timestamp: 2832.17,
          events: [
            {
              type: "shot",
              text: ["Adje!", "Nog 54 shotjes"],
              shotCount: 46,
            },
          ],
        },
        {
          timestamp: 2879,
          events: [
            {
              type: "song",
              text: ["Avicii", "My feelings for you"],
            },
          ],
        },
        {
          timestamp: 2891.35,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 53 shotjes"],
              shotCount: 47,
            },
          ],
        },
        {
          timestamp: 2909,
          events: [
            {
              type: "song",
              text: ["Richard Grey", "Volume at Last"],
            },
          ],
        },
        {
          timestamp: 2950.51,
          events: [
            {
              type: "shot",
              text: ["Bijna op de helft!", "Nog 52 shotjes"],
              shotCount: 48,
            },
            {
              type: "song",
              text: ["Chris brown", "Yeah 3x"],
            },
          ],
        },
        {
          timestamp: 2998,
          events: [
            {
              type: "song",
              text: ["Party Rock Anthem", "LMFAO"],
            },
            {
              type: "song",
              text: ["Party Rock Anthem", "Sexy and I Know it"],
            },
          ],
        },
        {
          timestamp: 3011.44,
          events: [
            {
              type: "shot",
              text: ["Bam!", "Nog 51 shotjes"],
              shotCount: 49,
            },
          ],
        },
        {
          timestamp: 3028,
          events: [
            {
              type: "song",
              text: ["Fatboy Slim", "Eat, Sleep, Rave, Repeat"],
            },
          ],
        },
        {
          timestamp: 3068.69,
          events: [
            {
              type: "shot",
              text: ["Abraham!", "Nog 50 shotjes"],
              shotCount: 50,
            },
          ],
        },
        {
          timestamp: 3086,
          events: [
            {
              type: "song",
              text: ["Chainsmokers", "#Selfie"],
            },
          ],
        },
        {
          timestamp: 3129.6,
          events: [
            {
              type: "shot",
              text: ["Tweede helft!", "Nog 49 shotjes"],
              shotCount: 51,
            },
          ],
        },
        {
          timestamp: 3186.84,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 48 shotjes"],
              shotCount: 52,
            },
            {
              type: "song",
              text: ["Eiffel 65", "Blue (da ba dee)"],
            },
          ],
        },
        {
          timestamp: 3245.92,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 47 shotjes"],
              shotCount: 53,
            },
            {
              type: "song",
              text: ["Baha man", "Who let the dogs out"],
            },
          ],
        },
        {
          timestamp: 3316.07,
          events: [
            {
              type: "shot",
              text: ["Zuipen!", "Nog 46 shotjes"],
              shotCount: 54,
            },
            {
              type: "song",
              text: ["Snollebollekes", "Snollebolleke"],
            },
          ],
        },
        {
          timestamp: 3368,
          events: [
            {
              type: "song",
              text: ["F*cking Vet !!! (Dag in, Dag uit)", "Britt"],
            },
          ],
        },
        {
          timestamp: 3369.6,
          events: [
            {
              type: "shot",
              text: ["Adje!", "Nog 45 shotjes"],
              shotCount: 55,
            },
          ],
        },
        {
          timestamp: 3406,
          events: [
            {
              type: "song",
              text: ["Deorro", "Bailar"],
            },
          ],
        },
        {
          timestamp: 3423.15,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 44 shotjes"],
              shotCount: 56,
            },
          ],
        },
        {
          timestamp: 3453,
          events: [
            {
              type: "song",
              text: ["O-zone", "Dragostea din tei"],
            },
          ],
        },
        {
          timestamp: 3482.23,
          events: [
            {
              type: "shot",
              text: ["Hard gaan!", "Nog 43 shotjes"],
              shotCount: 57,
            },
          ],
        },
        {
          timestamp: 3496,
          events: [
            {
              type: "song",
              text: ["André van Duin", "Paard in de gang"],
            },
            {
              type: "song",
              text: ["Stef ekkel", "Te dik in de kist"],
            },
          ],
        },
        {
          timestamp: 3542.19,
          events: [
            {
              type: "shot",
              text: ["Hap, slok, weg!", "Nog 42 shotjes"],
              shotCount: 58,
            },
            {
              type: "song",
              text: ["SRV Mannen", "Bier en tieten"],
            },
          ],
        },
        {
          timestamp: 3588,
          events: [
            {
              type: "song",
              text: ["Will.i.am ft. Britney Spears", "Scream & Shout"],
            },
            {
              type: "song",
              text: ["Helene Fischer", "Atemlos durch die Nacht"],
            },
          ],
        },
        {
          timestamp: 3621.23,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 41 shotjes"],
              shotCount: 59,
            },
          ],
        },
        {
          timestamp: 3664.86,
          events: [
            {
              type: "shot",
              text: ["Glas 60!", "Nog 40 shotjes"],
              shotCount: 60,
            },
            {
              type: "song",
              text: ["Sjaak ft. Mr. Polska", "Krokobil"],
            },
            {
              type: "song",
              text: ["Marco Mzee", "Der dj aus den Bergen"],
            },
          ],
        },
        {
          timestamp: 3699,
          events: [
            {
              type: "song",
              text: ["Tobee", "Helikopter 117"],
            },
          ],
        },
        {
          timestamp: 3722.54,
          events: [
            {
              type: "shot",
              text: ["Adje!", "Nog 39 shotjes"],
              shotCount: 61,
            },
          ],
        },
        {
          timestamp: 3737,
          events: [
            {
              type: "song",
              text: ["Pitbull ft. Ke$ha", "Timber"],
            },
          ],
        },
        {
          timestamp: 3793.65,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 38 shotjes"],
              shotCount: 62,
            },
            {
              type: "song",
              text: ["Avicii", "Wake me up"],
            },
          ],
        },
        {
          timestamp: 3840,
          events: [
            {
              type: "song",
              text: ["Axwell /\\ Ingrosso", "Sun is shining"],
            },
          ],
        },
        {
          timestamp: 3854.41,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 37 shotjes"],
              shotCount: 63,
            },
          ],
        },
        {
          timestamp: 3884,
          events: [
            {
              type: "song",
              text: ["DVBBS", "Tsunami"],
            },
          ],
        },
        {
          timestamp: 3915.04,
          events: [
            {
              type: "shot",
              text: ["Toe-toe-toeter!", "Nog 36 shotjes"],
              shotCount: 64,
            },
          ],
        },
        {
          timestamp: 3944,
          events: [
            {
              type: "song",
              text: ["Banny Benassi", "Satisfaction"],
            },
          ],
        },
        {
          timestamp: 3968,
          events: [
            {
              type: "song",
              text: ["De Jeugd van Tegenwoordig", "Watskeburt?!"],
            },
          ],
        },
        {
          timestamp: 3973.46,
          events: [
            {
              type: "shot",
              text: ["Tringeling!", "Nog 35 shotjes"],
              shotCount: 65,
            },
          ],
        },
        {
          timestamp: 4002,
          events: [
            {
              type: "song",
              text: ["Darude", "Sandstorm"],
            },
          ],
        },
        {
          timestamp: 4029.68,
          events: [
            {
              type: "shot",
              text: ["Zuipen!", "Nog 34 shotjes"],
              shotCount: 66,
            },
            {
              type: "song",
              text: ["A*Teens", "Mama mia"],
            },
          ],
        },
        {
          timestamp: 4036,
          events: [
            {
              type: "time",
              text: ["Plaspauze!", "Twee minuten tot volgende shot!"],
            },
          ],
        },
        {
          timestamp: 4100,
          events: [
            {
              type: "song",
              text: ["Earth wind & fire", "September"],
            },
          ],
        },
        {
          timestamp: 4155.41,
          events: [
            {
              type: "shot",
              text: ["Dorst!", "Nog 33 shotjes"],
              shotCount: 67,
            },
            {
              type: "song",
              text: ["Patrick hernandez", "Born to be alive"],
            },
          ],
        },
        {
          timestamp: 4180,
          events: [
            {
              type: "song",
              text: ["Jodi bernal", "Que si que no"],
            },
          ],
        },
        {
          timestamp: 4208.76,
          events: [
            {
              type: "shot",
              text: ["Biertje in de hand...", "Nog 32 shotjes"],
              shotCount: 68,
            },
          ],
        },
        {
          timestamp: 4241,
          events: [
            {
              type: "song",
              text: ["Jennifer lopez", "Let's get loud"],
            },
          ],
        },
        {
          timestamp: 4263.58,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 31 shotjes"],
              shotCount: 69,
            },
            {
              type: "song",
              text: ["Kinderen voor Kinderen", "Hallo Wereld"],
            },
          ],
        },
        {
          timestamp: 4294,
          events: [
            {
              type: "song",
              text: ["Snollebollekes", "Total loss"],
            },
          ],
        },
        {
          timestamp: 4321.57,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 30 shotjes"],
              shotCount: 70,
            },
            {
              type: "song",
              text: ["DJ Arnoud & DJ Jasper", "Soldaat"],
            },
          ],
        },
        {
          timestamp: 4337,
          events: [],
        },
        {
          timestamp: 4360,
          events: [
            {
              type: "song",
              text: ["André Hazes Jr.", "Leef"],
            },
          ],
        },
        {
          timestamp: 4384.43,
          events: [
            {
              type: "shot",
              text: ["Tik maar achterover!", "Nog 29 shotjes"],
              shotCount: 71,
            },
            {
              type: "song",
              text: ["Jeroen van der boom", "Jij bent zo"],
            },
          ],
        },
        {
          timestamp: 4424,
          events: [
            {
              type: "song",
              text: ["Crazy Frog", "Axel F"],
            },
          ],
        },
        {
          timestamp: 4450.4,
          events: [
            {
              type: "shot",
              text: ["Alcoholic party!", "Nog 28 shotjes"],
              shotCount: 72,
            },
            {
              type: "song",
              text: ["Scoop", "Drop it"],
            },
          ],
        },
        {
          timestamp: 4492,
          events: [
            {
              type: "song",
              text: ["Vengaboys", "We like to party"],
            },
          ],
        },
        {
          timestamp: 4503.85,
          events: [
            {
              type: "shot",
              text: ["Toet!", "Nog 27 shotjes"],
              shotCount: 73,
            },
          ],
        },
        {
          timestamp: 4533,
          events: [
            {
              type: "song",
              text: ["Special D.", "Home alone"],
            },
          ],
        },
        {
          timestamp: 4560.48,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 26 shotjes"],
              shotCount: 74,
            },
          ],
        },
        {
          timestamp: 4586,
          events: [
            {
              type: "song",
              text: ["Tim toupet", "Ich bin ein Döner"],
            },
          ],
        },
        {
          timestamp: 4615.33,
          events: [
            {
              type: "shot",
              text: ["Shotje 75!", "Nog 25 shotjes"],
              shotCount: 75,
            },
            {
              type: "song",
              text: ["Markus becker", "Helikopter"],
            },
          ],
        },
        {
          timestamp: 4677.05,
          events: [
            {
              type: "shot",
              text: ["Trek een ad!", "Nog 24 shotjes"],
              shotCount: 76,
            },
            {
              type: "song",
              text: ["Peter wackel", "Die Nacht von Freitag auf Monntag"],
            },
          ],
        },
        {
          timestamp: 4711,
          events: [
            {
              type: "song",
              text: ["Mama Laudaa", "Almklausi und Specktakel"],
            },
          ],
        },
        {
          timestamp: 4738.73,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 23 shotjes"],
              shotCount: 77,
            },
            {
              type: "song",
              text: ["Idina menzel", "Let it go"],
            },
          ],
        },
        {
          timestamp: 4796.17,
          events: [
            {
              type: "shot",
              text: ["scToeter!", "Nog 22 shotjes"],
              shotCount: 78,
            },
            {
              type: "song",
              text: ["Mr. polska", "Vlammen"],
            },
          ],
        },
        {
          timestamp: 4847,
          events: [
            {
              type: "song",
              text: ["Soca boys", "Follow the leader"],
            },
          ],
        },
        {
          timestamp: 4858.53,
          events: [
            {
              type: "shot",
              text: ["En door!", "Nog 21 shotjes"],
              shotCount: 79,
            },
          ],
        },
        {
          timestamp: 4874,
          events: [
            {
              type: "song",
              text: ["Het Feestteam", "Waar is dat feestje"],
            },
          ],
        },
        {
          timestamp: 4886,
          events: [
            {
              type: "song",
              text: ["FeestDjRuud", "Gas op die lollie"],
            },
          ],
        },
        {
          timestamp: 4913,
          events: [
            {
              type: "song",
              text: [
                "Snollebollekes",
                "Feest waarvan ik morgen niks meer weet",
              ],
            },
          ],
        },
        {
          timestamp: 4922.38,
          events: [
            {
              type: "shot",
              text: ["Al 80 in de mik!", "Nog 20 shotjes"],
              shotCount: 80,
            },
          ],
        },
        {
          timestamp: 4966,
          events: [
            {
              type: "song",
              text: ["Captain jack", "Captain jack"],
            },
          ],
        },
        {
          timestamp: 4979.75,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 19 shotjes"],
              shotCount: 81,
            },
          ],
        },
        {
          timestamp: 5000,
          events: [
            {
              type: "song",
              text: ["Can't hold us", "Macklemore"],
            },
          ],
        },
        {
          timestamp: 5046.51,
          events: [
            {
              type: "shot",
              text: ["Bakken vouwen!", "Nog 18 shotjes"],
              shotCount: 82,
            },
            {
              type: "song",
              text: ["Partyfriex", "Ik moet zuipen"],
            },
          ],
        },
        {
          timestamp: 5111.19,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 17 shotjes"],
              shotCount: 83,
            },
          ],
        },
        {
          timestamp: 5158.75,
          events: [
            {
              type: "shot",
              text: ["Let it bier!", "Nog 16 shotjes"],
              shotCount: 84,
            },
          ],
        },
        {
          timestamp: 5217.32,
          events: [
            {
              type: "shot",
              text: ["Adje numero 85!", "Nog 15 shotjes"],
              shotCount: 85,
            },
            {
              type: "song",
              text: ["Stefan en Sean", "Potentie (hardstyle remix)"],
            },
          ],
        },
        {
          timestamp: 5280.54,
          events: [
            {
              type: "shot",
              text: ["Toet!", "Nog 14 shotjes"],
              shotCount: 86,
            },
            {
              type: "song",
              text: ["Yellow Claw en Opposites", "Thunder"],
            },
          ],
        },
        {
          timestamp: 5320,
          events: [
            {
              type: "song",
              text: ["Buren van de brandweer", "Pilsies voor de vat"],
            },
          ],
        },
        {
          timestamp: 5345.3,
          events: [
            {
              type: "shot",
              text: ["Drinken!", "Nog 13 shotjes"],
              shotCount: 87,
            },
          ],
        },
        {
          timestamp: 5371,
          events: [
            {
              type: "song",
              text: ["Snollebollekes", "Links rechts"],
            },
          ],
        },
        {
          timestamp: 5408.47,
          events: [
            {
              type: "shot",
              text: ["I can't be your biero!", "Nog 12 shotjes"],
              shotCount: 88,
            },
          ],
        },
        {
          timestamp: 5460.59,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 11 shotjes"],
              shotCount: 89,
            },
            {
              type: "song",
              text: ["Mickie krause", "Nur noch die Schuhe an"],
            },
          ],
        },
        {
          timestamp: 5490,
          events: [
            {
              type: "song",
              text: ["Ina Colada", "Wodka mit Irgendwas"],
            },
          ],
        },
        {
          timestamp: 5525.35,
          events: [
            {
              type: "shot",
              text: ["90 in de keel, hierna niet veel!", "Nog 10 shotjes"],
              shotCount: 90,
            },
            {
              type: "song",
              text: ["Da tweekaz", "Jagermeister"],
            },
          ],
        },
        {
          timestamp: 5586.93,
          events: [
            {
              type: "shot",
              text: ["Zuip je uit de dubbele cijfers!", "Nog 9 shotjes"],
              shotCount: 91,
            },
            {
              type: "song",
              text: ["Zany & DV8", "Vreet Spirit"],
            },
          ],
        },
        {
          timestamp: 5628,
          events: [
            {
              type: "song",
              text: ["Jebroer", "Kind van de duivel"],
            },
          ],
        },
        {
          timestamp: 5651.67,
          events: [
            {
              type: "shot",
              text: ["Zuipen!", "Nog 8 shotjes"],
              shotCount: 92,
            },
          ],
        },
        {
          timestamp: 5678,
          events: [
            {
              type: "song",
              text: ["Dj Paul Elstak", "Promised land"],
            },
          ],
        },
        {
          timestamp: 5701.38,
          events: [
            {
              type: "shot",
              text: ["Pweeeep!", "Nog 7 shotjes"],
              shotCount: 93,
            },
          ],
        },
        {
          timestamp: 5726,
          events: [
            {
              type: "song",
              text: ["Dune", "Hardcore vibes"],
            },
          ],
        },
        {
          timestamp: 5764.24,
          events: [
            {
              type: "shot",
              text: ["Bijna daar!", "Nog 6 shotjes"],
              shotCount: 94,
            },
          ],
        },
        {
          timestamp: 5788,
          events: [
            {
              type: "song",
              text: ["A-ha", "Take on me"],
            },
          ],
        },
        {
          timestamp: 5822.41,
          events: [
            {
              type: "shot",
              text: ["Toeter!", "Nog 5 shotjes"],
              shotCount: 95,
            },
          ],
        },
        {
          timestamp: 5847,
          events: [
            {
              type: "song",
              text: ["Technohead", "I wanna be a hippy"],
            },
          ],
        },
        {
          timestamp: 5881,
          events: [
            {
              type: "song",
              text: ["Gabber piet", "Hakke en zage"],
            },
          ],
        },
        {
          timestamp: 5888.38,
          events: [
            {
              type: "shot",
              text: ["Slok 96!", "Nog 4 shotjes"],
              shotCount: 96,
            },
          ],
        },
        {
          timestamp: 5942.58,
          events: [
            {
              type: "shot",
              text: ["Voor de 97e keer!", "Nog 3 shotjes"],
              shotCount: 97,
            },
          ],
        },
        {
          timestamp: 5970,
          events: [
            {
              type: "song",
              text: ["Krezip", "I would stay"],
            },
          ],
        },
        {
          timestamp: 6004,
          events: [
            {
              type: "song",
              text: ["Scooter", "Bit a bad boy"],
            },
          ],
        },
        {
          timestamp: 6016.84,
          events: [
            {
              type: "shot",
              text: ["Nummer 98!", "Nog 2 shotjes"],
              shotCount: 98,
            },
          ],
        },
        {
          timestamp: 6063,
          events: [
            {
              type: "song",
              text: ["Bloed, zweet en tranen", "André Hazes"],
            },
          ],
        },
        {
          timestamp: 6071.34,
          events: [
            {
              type: "shot",
              text: ["Nummer 99!", "Nog 1 shotje"],
              shotCount: 99,
            },
          ],
        },
        {
          timestamp: 6127.81,
          events: [
            {
              type: "shot",
              text: ["CENTURION!", "Geen shots meer"],
              shotCount: 100,
            },
          ],
        },
      ],
    },
    {
      name: "Totale Isolatie",
      songFile: "songs/totale_isolatie.m4a",
      feed: [
        {
          timestamp: 0,
          events: [
            {
              type: "talk",
              text: ["Mondkapjes af!", "We gaan zo beginnen"],
            },
          ],
        },
        {
          timestamp: 2,
          events: [
            {
              type: "talk",
              text: [
                "Helaas hebben we nog geen lied-data :(",
                "Help ons: ict@svia.nl",
              ],
            },
          ],
        },
        {
          timestamp: 6.02,
          events: [
            {
              type: "shot",
              shotCount: 1,
              text: ["De eerste!", "Nog 41 shotjes"],
            },
          ],
        },
        {
          timestamp: 238,
          events: [
            {
              type: "shot",
              shotCount: 2,
              text: ["Nummertje twee!", "Nog 40 shotjes"],
            },
          ],
        },
        {
          timestamp: 478.6,
          events: [
            {
              type: "shot",
              shotCount: 3,
              text: ["Toeter!", "Nog 39 shotjes"],
            },
          ],
        },
        {
          timestamp: 541.43,
          events: [
            {
              type: "shot",
              shotCount: 4,
              text: ["Niet gooien!", "Nog 38 shotjes"],
            },
          ],
        },
        {
          timestamp: 656.18,
          events: [
            {
              type: "shot",
              shotCount: 5,
              text: ["Lustrum!", "Nog 37 shotjes"],
            },
          ],
        },
        {
          timestamp: 703.14,
          events: [
            {
              type: "shot",
              shotCount: 6,
              text: ["Toeter!", "Nog 36 shotjes"],
            },
          ],
        },
        {
          timestamp: 772.91,
          events: [
            {
              type: "shot",
              shotCount: 7,
              text: ["Pweeeep!", "Nog 35 shotjes"],
            },
          ],
        },
        {
          timestamp: 988.3,
          events: [
            {
              type: "shot",
              shotCount: 8,
              text: ["Trek een ad!", "Nog 34 shotjes"],
            },
          ],
        },
        {
          timestamp: 1015.39,
          events: [
            {
              type: "shot",
              shotCount: 9,
              text: ["We zijn er nog lang niet!", "Nog 33 shotjes"],
            },
          ],
        },
        {
          timestamp: 1129.51,
          events: [
            {
              type: "shot",
              shotCount: 10,
              text: ["Nummer tien!", "Nog 32 shotjes"],
            },
          ],
        },
        {
          timestamp: 1293.08,
          events: [
            {
              type: "shot",
              shotCount: 11,
              text: ["Met vriendelijke toet!", "Nog 31 shotjes"],
            },
          ],
        },
        {
          timestamp: 1320.67,
          events: [
            {
              type: "shot",
              shotCount: 12,
              text: ["Toeter!", "Nog 30 shotjes"],
            },
          ],
        },
        {
          timestamp: 1441.34,
          events: [
            {
              type: "shot",
              shotCount: 13,
              text: ["Ongeluksshotje 13!", "Nog 29 shotjes"],
            },
          ],
        },
        {
          timestamp: 1560.14,
          events: [
            {
              type: "shot",
              shotCount: 14,
              text: ["Pweeeep!", "Nog 28 shotjes"],
            },
          ],
        },
        {
          timestamp: 1742.03,
          events: [
            {
              type: "shot",
              shotCount: 15,
              text: ["Fünfzehn!", "Nog 27 shotjes"],
            },
          ],
        },
        {
          timestamp: 1912.31,
          events: [
            {
              type: "shot",
              shotCount: 16,
              text: ["Toet!", "Nog 26 shotjes"],
            },
          ],
        },
        {
          timestamp: 2040.11,
          events: [
            {
              type: "shot",
              shotCount: 17,
              text: ["Zuipen!", "Nog 25 shotjes"],
            },
          ],
        },
        {
          timestamp: 2076.69,
          events: [
            {
              type: "shot",
              shotCount: 18,
              text: ["18, legaal!", "Nog 24 shotjes"],
            },
          ],
        },
        {
          timestamp: 2121.42,
          events: [
            {
              type: "shot",
              shotCount: 19,
              text: ["Laatste shotje als tiener!", "Nog 23 shotjes"],
            },
          ],
        },
        {
          timestamp: 2261.79,
          events: [
            {
              type: "shot",
              shotCount: 20,
              text: ["Adje!", "Nog 22 shotjes"],
            },
          ],
        },
        {
          timestamp: 2311.63,
          events: [
            {
              type: "shot",
              shotCount: 21,
              text: ["Toeter!", "Nog 21 shotjes"],
            },
          ],
        },
        {
          timestamp: 2337.02,
          events: [
            {
              type: "shot",
              shotCount: 22,
              text: ["Toeter!", "Nog 20 shotjes"],
            },
          ],
        },
        {
          timestamp: 2396.28,
          events: [
            {
              type: "shot",
              shotCount: 23,
              text: ["Claxon!", "Nog 19 shotjes"],
            },
          ],
        },
        {
          timestamp: 2557.79,
          events: [
            {
              type: "shot",
              shotCount: 24,
              text: ["En nu even op standje maximaal!", "Nog 18 shotjes"],
            },
          ],
        },
        {
          timestamp: 2696.85,
          events: [
            {
              type: "shot",
              shotCount: 25,
              text: ["Halve Abraham!", "Nog 17 shotjes"],
            },
          ],
        },
        {
          timestamp: 2743.54,
          events: [
            {
              type: "shot",
              shotCount: 26,
              text: ["Hoch die Hände!", "Nog 16 shotjes"],
            },
          ],
        },
        {
          timestamp: 2798.76,
          events: [
            {
              type: "shot",
              shotCount: 27,
              text: ["Voel je 'm al?", "Nog 15 shotjes"],
            },
          ],
        },
        {
          timestamp: 2961.7,
          events: [
            {
              type: "shot",
              shotCount: 28,
              text: ["Project X!", "Nog 14 shotjes"],
            },
          ],
        },
        {
          timestamp: 2985.08,
          events: [
            {
              type: "shot",
              shotCount: 29,
              text: ["Pweeeep!", "Nog 13 shotjes"],
            },
          ],
        },
        {
          timestamp: 3095.67,
          events: [
            {
              type: "shot",
              shotCount: 30,
              text: ["Toet!", "Nog 12 shotjes"],
            },
          ],
        },
        {
          timestamp: 3139.89,
          events: [
            {
              type: "shot",
              shotCount: 31,
              text: ["Over de dertig!", "Nog 69 (nice) shotjes"],
            },
          ],
        },
        {
          timestamp: 3207.67,
          events: [
            {
              type: "shot",
              shotCount: 32,
              text: ["Trek een ad!", "Nog 10 shotjes"],
            },
          ],
        },
        {
          timestamp: 3286.54,
          events: [
            {
              type: "shot",
              shotCount: 33,
              text: ["Nummertje 33!", "Nog 9 shotjes!"],
            },
          ],
        },
        {
          timestamp: 3332.63,
          events: [
            {
              type: "shot",
              shotCount: 34,
              text: ["Voor het vaderland!", "Nog 8 shotjes"],
            },
          ],
        },
        {
          timestamp: 3452.63,
          events: [
            {
              type: "shot",
              shotCount: 35,
              text: ["Toeter!", "Nog 7 shotjes"],
            },
          ],
        },
        {
          timestamp: 3503.85,
          events: [
            {
              type: "shot",
              shotCount: 36,
              text: ["Pweeeep!", "Nog 6 shotjes"],
            },
          ],
        },
        {
          timestamp: 3740.03,
          events: [
            {
              type: "shot",
              shotCount: 37,
              text: ["In dat keelgaatje!", "Nog 5 shotjes"],
            },
          ],
        },
        {
          timestamp: 3806.65,
          events: [
            {
              type: "shot",
              shotCount: 38,
              text: ["Toeter!", "Nog 4 shotjes"],
            },
          ],
        },
        {
          timestamp: 3833.54,
          events: [
            {
              type: "shot",
              shotCount: 39,
              text: ["Toet!", "Nog 3 shotjes"],
            },
          ],
        },
        {
          timestamp: 3876.81,
          events: [
            {
              type: "shot",
              shotCount: 40,
              text: ["40 alweer!", "Nog 2 shotjes"],
            },
          ],
        },
        {
          timestamp: 4044.97,
          events: [
            {
              type: "shot",
              shotCount: 41,
              text: ["Zuipen!", "Nog 1 shotje"],
            },
          ],
        },
        {
          timestamp: 4069.1,
          events: [
            {
              type: "shot",
              shotCount: 42,
              text: ["Volledige eenzaamheid!!", "Geen shots meer"],
            },
          ],
        },
      ],
    },
  ],
};

export default timelines;
