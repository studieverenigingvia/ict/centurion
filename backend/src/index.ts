import express from "express";
import { Server } from "socket.io";
import path from "path";

import Service from "./Service";
import { RoomOptions } from "./Room";

const HOST = "0.0.0.0";
const PORT = 3001;

const app = express();
const httpServer = app.listen(PORT, HOST, () =>
  console.log(`Centurion listening on port ${PORT}!`)
);
app.use(express.static(path.join(__dirname, "../public")));

const io = new Server(httpServer);

const service = new Service();

app.get("/state", (req, res) => {
  return res.json(service.rooms.map((r) => r.serialize()));
});

io.on("connection", (socket) => {
  socket.on("disconnect", async () => {
    await service.onSocketDisconnect(socket);
  });

  socket.on("ping", () => {
    socket.emit("pong");
  });

  socket.on("time_sync", (requestId: number, clientTime: number) => {
    if (!Number.isSafeInteger(requestId)) return;
    if (!Number.isSafeInteger(clientTime)) return;

    service.onTimeSync(socket, requestId, clientTime);
  });

  socket.on("room_options", (options: RoomOptions) => {
    if (!options) return;
    if (!options.timelineName || typeof options.timelineName !== "string")
      return;
    if (!Number.isSafeInteger(options.seekTime)) return;

    service.onSetRoomOptions(socket, options);
  });

  socket.on("request_start", () => {
    service.onRequestStart(socket);
  });

  socket.on(
    "request_join",
    async (
      roomId: number,
      callback?: (err?: string, res?: boolean) => void
    ) => {
      if (!Number.isSafeInteger(roomId)) {
        return callback?.("Invalid roomId.");
      }

      if (!service.hasRoomId(roomId)) {
        // cannot join a room that does not exist.
        return callback?.(undefined, false);
      }

      try {
        const didJoinRoom = await service.onRequestJoin(socket, roomId);
        callback?.(undefined, didJoinRoom);
      } catch (e) {
        callback?.(e instanceof Error ? e.message : "Unknown error.");
      }
    }
  );

  socket.on("request_set_ready", () => {
    service.onRequestSetReady(socket);
  });

  socket.on("request_join_random", async () => {
    await service.onRequestJoinRandom(socket);
  });

  socket.on(
    "submit_ticker_message",
    (message?: unknown, callback?: (res?: null, err?: string) => void) => {
      if (typeof message !== "string") {
        return callback && callback(null, "Invalid message.");
      }

      if (message.length > 192) {
        // perfect voor het Wilhelmus
        return callback && callback(null, "Message too long.");
      }

      try {
        service.submitTickerMessage(socket, message);
        return callback && callback();
      } catch (e) {
        console.error(e);
        return (
          callback &&
          callback(null, e instanceof Error ? e.message : "Unknown error")
        );
      }
    }
  );

  service.onSocketConnect(socket);
});
