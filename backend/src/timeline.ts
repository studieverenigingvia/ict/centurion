import timeline from "./data/timelines";

export function getTimelineNames(): string[] {
  return timeline.timelines.map((timeline) => timeline.name);
}

export function getTimeline(name: string) {
  const t = timeline.timelines.find((t) => t.name == name);
  if (!t) return null;
  return t;
}
