CENTURION
=========

## Introduction
The projects consists of two projects, a frontend written in React, and a backend written in Typescript (Node). Both communicate with a websocket.

## Setup
1. Download the various song files and place them in `frontend/public/songs/`. Easiest way to do this is to get them from production (E.g. `https://centurion.svia.nl/songs/centurion.m4a`)
2. Run `npm i` in the root directory
3. After root directory installed, run `npm i` in frontend/, and backend/.
4. Prepare the pre-commit hooks by running `npm run prepare`.

ESLint is used for linting, Prettier for formatting. Currently, there are no tests. Tread carefully.

## Starting

* Start the frontend by running `npm run start` in the frontend directory.
* Idem for the backend.





