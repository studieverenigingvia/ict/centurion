{{/*
Expand the name of the chart.
*/}}
{{- define "centurion.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "centurion.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "centurion.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "centurion.labels" -}}
helm.sh/chart: {{ include "centurion.chart" . }}
{{ include "centurion.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "centurion.selectorLabels" -}}
app.kubernetes.io/name: {{ include "centurion.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the environment variables used by the app
*/}}
{{- define "centurion.appEnvironment" }}
{{- end }}


{{/*
App volumes and volumeMounts for secret files (api keys).
*/}}
{{- define "centurion.volumeMounts" -}}
- name: centurion-songs
  mountPath: /usr/share/nginx/html/songs/
{{- end }}

{{- define "centurion.volumes" -}}
- name: centurion-songs
  persistentVolumeClaim:
    claimName: {{ include "centurion.fullname" . }}-songs
{{- end }}

